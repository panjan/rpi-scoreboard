extern crate rppal;

use std::thread;
use std::sync::mpsc;
use std::time::Duration;
use std::{
    fs::File,
    io::{BufWriter, Write},
};

use rppal::gpio::{Gpio, Level, Mode};
use rppal::system::DeviceInfo;

const GPIO_PIN_CLK: u8 = 17;
const GPIO_PIN_DAT: u8 = 18;

fn main() {
    match run() {
        Ok(_) => {}
        e => {
            eprintln!("Error: {:?}", e);
        }
    }
}

fn run() -> Result<(), Box<::rppal::gpio::Error>> {
    let device_info = DeviceInfo::new().unwrap();
    println!(
        "Model: {} (SoC: {})",
        device_info.model(),
        device_info.soc()
    );


    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let gpio = Gpio::new().unwrap();

        let clk_pin = gpio.get(GPIO_PIN_CLK).unwrap().into_input();
        let dat_pin = gpio.get(GPIO_PIN_DAT).unwrap().into_input();

        let mut last_clk_state = Level::High;

        loop {
            match clk_pin.read() {
                Level::High => if last_clk_state == Level::Low {
                    if dat_pin.is_low() {
                        tx.send(1).unwrap();
                    } else {
                        tx.send(-1).unwrap();
                    }

                    last_clk_state = Level::High;
                },
                state => {
                    last_clk_state = state;
                }
            }

            thread::sleep(Duration::from_micros(100));
        }
    });

    let mut count = 0;
    for received in rx {
        count += received;
        println!("Got: {} for a count of {}", received, count);
        let write_file = File::create("./encoder_data").unwrap();
        let mut writer = BufWriter::new(&write_file);
        write!(&mut writer,"Count {}", count);
        // let mut persistent_file = File::create("encoder_data.txt").unwrap();
        // write!(persistent_file, "Count:{}", count);
    }

    Ok(())
}