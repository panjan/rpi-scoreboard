#!/bin/bash

wget https://github.com/caddyserver/caddy/releases/download/v2.5.0/caddy_2.5.0_linux_armv7.tar.gz
tar xvf caddy_2.5.0_linux_armv7.tar.gz
cp caddy /usr/local/bin/caddy
