#![feature(entry_insert)]

extern crate rppal;
extern crate mono_display;

use std::env;
use std::collections::HashMap;
use std::thread;
use std::sync::mpsc;
use std::time::Duration;
use std::{
    fs::File,
    io::{BufWriter, Write, Read},
};
use regex::Regex;
use mono_display::gfx;
use mono_display::DisplayDriver;
use rppal::gpio::{Gpio, Level};
use rppal::system::DeviceInfo;

fn main() {
    match run() {
        Ok(_) => {}
        e => {
            eprintln!("Error: {:?}", e);
        }
    }
}

#[cfg(feature = "sdl2")]
fn create_display_driver() -> mono_display::Result<mono_display::sdl_driver::SdlDriver> {
    mono_display::sdl_driver::SdlDriver::new(gfx::Size::wh(128, 32))
}

#[cfg(not(feature = "sdl2"))]
fn create_display_driver() -> mono_display::Result<mono_display::ssd1306::Ssd1306> {
    mono_display::ssd1306::Ssd1306::new(mono_display::ssd1306::Ssd1306Type::S128x32, false)
}


fn run() -> Result<(), Box<::rppal::gpio::Error>> {
    // encoder connection pin definition
    let clk_pins = vec![12, 16, 20, 5];
    let dat_pins = vec![6, 19, 26, 13];
    let device_info = DeviceInfo::new().unwrap();
    println!(
        "Model: {} (SoC: {})",
        device_info.model(),
        device_info.soc()
    );
    let mut display_driver = create_display_driver().unwrap();
    let args: Vec<String> = env::args().collect();
    let output_filename = args[2].as_str();  
    let font16 = gfx::Font::load_bdf(args[1].as_str()).unwrap();
    // let font16 = gfx::Font::load_bdf("./fonts/IBMBIOS8x8-8.bdf")?;
    let text_pos = gfx::Vector::xy(14, 30); 
    let encoders_count = 4;

    let (tx, rx) = mpsc::channel();

    for n in 0..encoders_count {
        let txclone = tx.clone();
        let clkp = clk_pins.clone();
        let datp = dat_pins.clone();

        thread::spawn(move || {
            let gpio = Gpio::new().unwrap();

            let clk_pin = gpio.get(clkp[n]).unwrap().into_input();
            let dat_pin = gpio.get(datp[n]).unwrap().into_input();

            let mut last_clk_state = Level::High;

            loop {
                match clk_pin.read() {
                    Level::High => if last_clk_state == Level::Low {
                        if dat_pin.is_low() {
                            txclone.send((n,1)).unwrap();
                        } else {
                            txclone.send((n,-1)).unwrap();
                        }

                        last_clk_state = Level::High;
                    },
                    state => {
                        last_clk_state = state;
                    }
                }
                thread::sleep(Duration::from_micros(100));
            }
        });
    }

    // read former state from file
    let mut counter = HashMap::new();
    match File::open(output_filename) {
        Ok(mut file) => {
            let mut contents = String::new();
            file.read_to_string(&mut contents).unwrap();
            let re = Regex::new(r"(\d+)").unwrap();
            // println!("{:?}",contents);
            let mut vec = Vec::new();
            vec =
            re.captures_iter(&contents)
              .map(|c| c[0].parse::<i32>().unwrap())
              .collect();
            // println!("{:?}, {:?}",vec, vec.len());

            for n in 0..vec.len()/2 {
                println!("{:?}",n);
                counter.insert(vec[(n*2)].to_string(),vec[(n*2)+1]);
            }
        },
        Err(_) => println!("There was no previos state.")
    }
    
    println!("Starting state: {:?}", counter);

    // let mut counter = HashMap::new();
    
    for received in rx {
        let (id, amount) = received; 
        counter
            .entry(id.to_string())
            .and_modify(|count| *count += amount)
            .or_insert(0);
        println!("Got: {} from {} a counter {:?}", amount, id, counter);
        // write!(&mut writer,"{:?}", "hello");
        let write_file = File::create(output_filename).unwrap();
        let mut writer = BufWriter::new(&write_file);
        match write!(&mut writer,"{:?}", counter) {
            Ok(_) => println!("written, {:?}", counter),
            Err(e) => println!("Error! {:?}", e),
        }
        // if let Err(err) = write!(&mut writer,"{:?}", counter) {
        //      println!("Error! {:?}", err);
        // }
        let tmp_counter = counter.clone();
        let mut canvas = gfx::Canvas::new(display_driver.get_frame());
        match tmp_counter.get(&id.to_string()) {
            Some(&number) => canvas.draw_text(text_pos, &font16, &format!("{}", number), gfx::Color::Light),
            _ => println!("Something went teribly wrong."),
        }
        display_driver.show_frame(canvas.take_frame());
    }

    Ok(())
}