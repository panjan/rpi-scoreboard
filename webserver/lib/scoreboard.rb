# frozen_string_literal: true

require_relative "scoreboard/version"

module Scoreboard
  class Error < StandardError; end
  # Your code goes here...
end
